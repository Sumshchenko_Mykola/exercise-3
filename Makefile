all: static

static: main.o double.o dice.o
	gcc main.o double.o dice.o -o dice
	rm -rf *.o

dynamic: main.o double.o dice.o
	ar -r libdice.a dice.o
	gcc main.o double.o -o dice -static -L./ -ldice
	rm -rf *.o

main.o:
	gcc -c main.c

dice.o:
	gcc -c dice.c

double.o:
	gcc -c ./Libraries/DoublyLinkedList.c -o double.o

clean:
	rm -rf *.o *.a dice
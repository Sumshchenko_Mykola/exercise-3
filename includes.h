#ifndef INCLUDES_H
#define INCLUDES_H

#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Libraries/DoublyLinkedList.h"

#define WON 1
#define LOSE 0

int rollDice(void);
int playGame(bool isPlayer);
void gameLoop();

#endif

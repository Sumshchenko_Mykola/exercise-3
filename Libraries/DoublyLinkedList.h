struct Match {
    int playerScore;
    int enemyScore;
    char* time;
};

struct Node { 
    struct Match data; 
    struct Node* next;
    struct Node* prev;
};

void push(struct Node** head_ref, struct Match new_data);
void insertAfter(struct Node* prev_node, struct Match new_data);
void append(struct Node** head_ref, struct Match new_data);
#include "includes.h"


void initRandomize() {
    time_t t;
    srand(time(&t)); // Initialize random seed
}

int rollDice(void) {
    return ((rand() % 6) + 1);
}

char* getTime() {
    time_t rawtime;
    struct tm * timeinfo;

    time( &rawtime );
    timeinfo = localtime(&rawtime);
    return asctime(timeinfo);
}

char* getName() {
    char* line = NULL;
    size_t len = 0;
    getline(&line, &len, stdin);
    return line;
}

int playGame(bool isPlayer) {
    int dice_1 = 0;
    int dice_2 = 0;
    int sum = 0;
    int result;
    int point = 0;
    bool playForPoint = false;

    initRandomize();

    if (isPlayer) {
        printf("ROLL THE DICE WITH [ENTER]\n");
        fgetc(stdin);
    } else {
        printf("Eric is playing:\n");
    }

    dice_1 = rollDice();
    dice_2 = rollDice();
    sum = dice_1 + dice_2;
    printf("D1:%2d - D2:%2d - Sum:%2d\n", dice_1, dice_2, sum);

    switch ( sum )
    {
        case 7:
        case 11:
            result = WON;
            break;
        case 2:
        case 3:
        case 12:
            result = LOSE;
            break;
        default:
            playForPoint = true;
            point = sum;
            printf("Playing for point:%d.\n", point);
            if (isPlayer) {
                printf("Please hit enter.\n");
                fgetc(stdin);
            }
            break;
    }

    while ( playForPoint )
    {
        dice_1 = rollDice();
        dice_2 = rollDice();
        sum = dice_1 + dice_2;
        printf("D1:%2d - D2:%2d - Sum:%2d\n", dice_1, dice_2, sum);
        if ( sum == 7 ) {
            playForPoint = false;
            result = LOSE;
        } else if ( sum == point ) {
            playForPoint = false;
            result = WON;
        } else {
            if (isPlayer) {
                printf("Please roll the dice again with [ENTER].\n");
                fgetc(stdin);
            }
        }
    }

    return result;
}

void printResult(int result, bool isPlayer) {
    char* name;
    if(isPlayer) {
        name = "+++You";
    } else {
        name = "---Eric";
    }

    switch(result)
    {
        case WON:
            printf("%s won the game.\n", name);
            break;
        case LOSE:
            printf("%s lost the game.\n", name);
            break;
        default:
            printf("Something went wrong in the program.\n");
            break;
    }
}

void endGame(struct Node* node) {
    struct Node* last;
    struct Node* first = node;
    int playerScore;
    int enemyScore;
    int totalPlayer;
    int totalEnemy;
    printf("Your score: "); 
    while (node != NULL) {
        playerScore = node->data.playerScore;
        printf(" %d ", playerScore); 
        totalPlayer += playerScore;
        last = node; 
        node = node->next; 
    }
    printf("\n");
    node = first;
    printf("Eric score: ");
    while (node != NULL) {
        enemyScore = node->data.enemyScore;
        printf(" %d ", enemyScore);
        totalEnemy += enemyScore;
        last = node; 
        node = node->next; 
    }
    printf("\n");
    printf("\n");

    printf("Your total score: %d \n", totalPlayer);
    printf("Eric total score: %d \n", totalEnemy);
}

void gameLoop() {
    printf ("Please type your name: ");
    printf("Your name is: %s", getName());
    char* matchTime;
    int playerResult;
    int enemyResult;
    bool isPlayer;
    bool keepPlaying = true;
    struct Node* head = NULL;

    while (keepPlaying) {
        matchTime = getTime();
        isPlayer = true;
        int playerResult = playGame(isPlayer);
        printResult(playerResult, isPlayer);

        isPlayer = false;
        int enemyResult = playGame(isPlayer);
        printResult(enemyResult, isPlayer);

        struct Match match = { playerResult, enemyResult, matchTime };
        push(&head, match);


        printf ("Do you want to play one more time? y/n \n");
        char c = getchar();
        if (c == 'n' || c == 'N') {
            keepPlaying = false;
            endGame(head);
        }
    }
}